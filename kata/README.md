# Customer Delivery Details API

This project provides an API for managing customer delivery details using Spring Boot.

## Setup Instructions

### Prerequisites
- Java 17 or higher installed
- Maven installed
- SQL database (e.g., MySQL)


### Getting Started

1. Clone the repository:

2. Create a SQL Database:
- Create a database named `delivery_db` in your SQL server.

3. Configure Database Connection:
- Open `src/main/resources/application.properties`.
- Update the database connection properties (URL, username, password) as needed.

4. Run the application:

The application will start at `http://localhost:8080`.

5. Test the API using Postman:
- Open Postman and import the provided collection file (`customer-delivery-details.postman_collection.json`).
- Use the collection to test various endpoints (create, read, update, delete) for customer delivery details.

## API Endpoints

- `GET /api/v1/delivery-details`: Get all customer delivery details.
- `GET /api/v1/delivery-details/{id}`: Get a specific delivery detail by ID.
- `POST /api/v1/delivery-details`: Create a new delivery detail.
- `PUT /api/v1/delivery-details/{id}`: Update a delivery detail.
- `DELETE /api/v1/delivery-details/{id}`: Delete a delivery detail.