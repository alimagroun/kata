package com.magroun.kata.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.magroun.kata.model.CustomerDeliveryDetails;

@Repository
public interface CustomerDeliveryDetailsRepository extends JpaRepository<CustomerDeliveryDetails, Long> {
 
}
