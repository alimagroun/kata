package com.magroun.kata.service.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magroun.kata.model.CustomerDeliveryDetails;
import com.magroun.kata.repository.CustomerDeliveryDetailsRepository;
import com.magroun.kata.service.CustomerDeliveryDetailsService;

import java.util.List;

@Service
public class CustomerDeliveryDetailsServiceImpl implements CustomerDeliveryDetailsService {

    private final CustomerDeliveryDetailsRepository detailsRepository;

    @Autowired
    public CustomerDeliveryDetailsServiceImpl(CustomerDeliveryDetailsRepository detailsRepository) {
        this.detailsRepository = detailsRepository;
    }

    @Override
    public CustomerDeliveryDetails createCustomerDeliveryDetails(CustomerDeliveryDetails newDetails) {
        return detailsRepository.save(newDetails);
    }

    @Override
    public List<CustomerDeliveryDetails> getAllCustomerDeliveryDetails() {
        return detailsRepository.findAll();
    }

    @Override
    public CustomerDeliveryDetails getCustomerDeliveryDetailsById(Long id) {
        return detailsRepository.findById(id).orElse(null);
    }

    @Override
    public CustomerDeliveryDetails updateCustomerDeliveryDetails(Long id, CustomerDeliveryDetails updatedDetails) {
        if (detailsRepository.existsById(id)) {
            updatedDetails.setId(id);
            return detailsRepository.save(updatedDetails);
        }
        return null;
    }

    @Override
    public void deleteCustomerDeliveryDetailsById(Long id) {
        detailsRepository.deleteById(id);
    }
}

