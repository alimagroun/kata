package com.magroun.kata.service;

import java.util.List;

import com.magroun.kata.model.Customer;

public interface CustomerService {
	
    Customer getCustomerById(Long customerId);

    List<Customer> getAllCustomers();

    Customer createCustomer(Customer newCustomer);

    Customer updateCustomer(Long customerId, Customer updatedCustomer);

    void deleteCustomerById(Long customerId);

}
