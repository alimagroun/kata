package com.magroun.kata.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magroun.kata.model.CustomerDeliveryDetails;
import com.magroun.kata.service.CustomerDeliveryDetailsService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/delivery-details")
public class CustomerDeliveryDetailsController {

    private final CustomerDeliveryDetailsService detailsService;

    @Autowired
    public CustomerDeliveryDetailsController(CustomerDeliveryDetailsService detailsService) {
        this.detailsService = detailsService;
    }

    @GetMapping
    public ResponseEntity<List<CustomerDeliveryDetails>> getAllCustomerDeliveryDetails() {
        List<CustomerDeliveryDetails> detailsList = detailsService.getAllCustomerDeliveryDetails();
        return new ResponseEntity<>(detailsList, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerDeliveryDetails> getCustomerDeliveryDetailsById(@PathVariable Long id) {
        CustomerDeliveryDetails details = detailsService.getCustomerDeliveryDetailsById(id);
        if (details != null) {
            return new ResponseEntity<>(details, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public ResponseEntity<CustomerDeliveryDetails> createCustomerDeliveryDetails(@RequestBody CustomerDeliveryDetails details) {
        CustomerDeliveryDetails createdDetails = detailsService.createCustomerDeliveryDetails(details);
        return new ResponseEntity<>(createdDetails, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CustomerDeliveryDetails> updateCustomerDeliveryDetails(
            @PathVariable Long id, @RequestBody CustomerDeliveryDetails updatedDetails) {
        CustomerDeliveryDetails updated = detailsService.updateCustomerDeliveryDetails(id, updatedDetails);
        if (updated != null) {
            return new ResponseEntity<>(updated, HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteCustomerDeliveryDetailsById(@PathVariable Long id) {
        detailsService.deleteCustomerDeliveryDetailsById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}

