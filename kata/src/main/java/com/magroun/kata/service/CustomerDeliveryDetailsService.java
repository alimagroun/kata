package com.magroun.kata.service;

import java.util.List;

import com.magroun.kata.model.CustomerDeliveryDetails;

public interface CustomerDeliveryDetailsService {
    CustomerDeliveryDetails createCustomerDeliveryDetails(CustomerDeliveryDetails newDetails);
    List<CustomerDeliveryDetails> getAllCustomerDeliveryDetails();
    CustomerDeliveryDetails getCustomerDeliveryDetailsById(Long id);
    CustomerDeliveryDetails updateCustomerDeliveryDetails(Long id, CustomerDeliveryDetails updatedDetails);
    void deleteCustomerDeliveryDetailsById(Long id);
}

