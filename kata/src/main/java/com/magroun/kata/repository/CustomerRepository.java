package com.magroun.kata.repository;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.magroun.kata.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
